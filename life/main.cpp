#include "pch.h"

#include <windows.h>

#include "basewindow.h"
#include "lifegame.h"
#include "game_ingine.h"

#pragma comment(lib, "d2d1")

// the entry point
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	HeapSetInformation(NULL, HeapEnableTerminationOnCorruption, NULL, 0);

	UINT size = 10;

	UINT x = 1200 / size;
	UINT y = 800 / size;

	if (SUCCEEDED(CoInitialize(NULL)))
	{
		LifeGameEngine gameEngine(x, y);
		LifeGameWindow window(&gameEngine, size);

		if (SUCCEEDED(window.Initialize(L"Life : Game", WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX, x * size, y * size)))
		{
			window.RunMessageLoop();
		}

		CoUninitialize();
	}

	return 0;
};