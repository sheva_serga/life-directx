#include "pch.h"

#include <windows.h>

#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <wchar.h>
#include <math.h>

#include <d2d1.h>
#include <d2d1helper.h>
#include <dwrite.h>
#include <wincodec.h>

#include "game_ingine.h"
#include "saferelease.h"
#include "lifegame.h"

LifeGameWindow::LifeGameWindow(LifeGameEngine* m_gaeniEngine, UINT ractsize) :
	m_pDirect2dFactory(NULL),
	m_pRenderTarget(NULL),
	m_pGreenBrush(NULL),
	m_pBlackBrush(NULL),
	m_pWhiteBrush(NULL),
	m_pGameEngine(m_gaeniEngine),
	m_pRactSize(ractsize), 
	m_pRenderStopped(false) { }

LifeGameWindow::~LifeGameWindow()
{
	SafeRelease(&m_pDirect2dFactory);
	SafeRelease(&m_pRenderTarget);
	SafeRelease(&m_pBlackBrush);
	SafeRelease(&m_pGreenBrush);
	SafeRelease(&m_pWhiteBrush);
}

BOOL LifeGameWindow::Initialize(PCWSTR lpWindowName, DWORD dwStyle, int nWidth, int nHeight, DWORD dwExStyle, int x, int y, HWND hWndParent, HMENU hMenu)
{
	HRESULT hr;

	// Initialize device-indpendent resources, such
	// as the Direct2D factory.
	hr = CreateDeviceIndependentResources();

	if (SUCCEEDED(hr))
	{
		FLOAT dpiX, dpiY;

		// The factory returns the current system DPI. This is also the value it will use
		// to create its own windows.
		m_pDirect2dFactory->GetDesktopDpi(&dpiX, &dpiY);

		if (BaseWindow::Initialize(lpWindowName, dwStyle, nWidth ,nHeight))
		{
			ShowWindow(m_hwnd, SW_SHOWNORMAL);
			UpdateWindow(m_hwnd);
		}

		hr = m_hwnd ? S_OK : E_FAIL;
	}

	return hr;
}

PCWSTR LifeGameWindow::ClassName() const { return L"LifeGameWindow"; }

LRESULT LifeGameWindow::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
		case WM_LBUTTONDOWN: {
			if (m_pRenderStopped)
			{
				WORD x, y;

				x = LOWORD(lParam);
				y = HIWORD(lParam);

				UINT yElemIndex = (y - y % m_pRactSize) / m_pRactSize;
				UINT xElemIndex = (x - x % m_pRactSize) / m_pRactSize;

				m_pGameEngine->SetLifeToArea(xElemIndex, yElemIndex);
				OnRender();
			}

			return 0L;
		} break;
		case WM_KEYDOWN: {
			if (wParam == 's' || wParam == 'S')
				m_pRenderStopped = !m_pRenderStopped;

			if (wParam == 'c' || wParam == 'C')
			{
				m_pGameEngine->Clear();
				OnRender();
			}

			return 0L;
		} break;
		case WM_DESTROY: {
			PostQuitMessage(0);

			return 0;
		} break;
		case WM_SIZE: {
			UINT width = LOWORD(lParam);
			UINT height = HIWORD(lParam);

			OnResize(width, height);
			
			return 0;
		} break;
		case WM_DISPLAYCHANGE: {
			OnRender();
			ValidateRect(m_hwnd, NULL);

			return 0;
		}
		case WM_PAINT:
		{
			OnRender();
			ValidateRect(m_hwnd, NULL);

			return 0;
		} break;
		default: {
			return DefWindowProc(m_hwnd, uMsg, wParam, lParam);
		} break;
	}
}

HRESULT LifeGameWindow::CreateDeviceIndependentResources()
{
	HRESULT hr = S_OK;

	// Create a Direct2D factory.
	hr = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &m_pDirect2dFactory);

	return hr;
}

HRESULT LifeGameWindow::CreateDeviceResources()
{
	HRESULT hr = S_OK;

	if (!m_pRenderTarget)
	{
		RECT rc;
		GetClientRect(m_hwnd, &rc);

		D2D1_SIZE_U size = D2D1::SizeU(
			rc.right - rc.left,
			rc.bottom - rc.top
		);

		// Create a Direct2D render target.
		hr = m_pDirect2dFactory->CreateHwndRenderTarget(
			D2D1::RenderTargetProperties(),
			D2D1::HwndRenderTargetProperties(m_hwnd, size),
			&m_pRenderTarget
		);

		if (SUCCEEDED(hr))
		{
			// Create a blue brush.
			hr = m_pRenderTarget->CreateSolidColorBrush(
				D2D1::ColorF(D2D1::ColorF::Green),
				&m_pGreenBrush
			);
		}

		if (SUCCEEDED(hr)) {
			hr = m_pRenderTarget->CreateSolidColorBrush(
				D2D1::ColorF(D2D1::ColorF::Black),
				&m_pBlackBrush
			);
		}

		if (SUCCEEDED(hr)) {
			hr = m_pRenderTarget->CreateSolidColorBrush(
				D2D1::ColorF(D2D1::ColorF::White),
				&m_pWhiteBrush
			);
		}
	}

	return hr;
}

HRESULT LifeGameWindow::OnRender()
{
	HRESULT hr = S_OK;

	hr = CreateDeviceResources();

	if (SUCCEEDED(hr))
	{
		m_pRenderTarget->BeginDraw();
		m_pRenderTarget->SetTransform(D2D1::Matrix3x2F::Identity());
		m_pRenderTarget->Clear(D2D1::ColorF(D2D1::ColorF::White));

		D2D1_SIZE_F rtSize = m_pRenderTarget->GetSize();
		
		int width = static_cast<int>(rtSize.width);
		int height = static_cast<int>(rtSize.height);

		UINT y_size = m_pGameEngine->y_size;
		UINT x_sizw = m_pGameEngine->x_size;

		bool** currentState = m_pGameEngine->GetCurrentState();

		D2D1_RECT_F rectangle;

		for (size_t y = 0; y < y_size; y++)
			for (size_t x = 0; x < x_sizw; x++)
			{
				if (currentState[y][x]) 
				{
					rectangle = D2D1::RectF(x * m_pRactSize, y * m_pRactSize, x * m_pRactSize + m_pRactSize, y * m_pRactSize + m_pRactSize);
					m_pRenderTarget->FillRectangle(&rectangle, m_pGreenBrush);
					m_pRenderTarget->DrawRectangle(&rectangle, m_pGreenBrush);
				}
			}

		for (int x = 0; x < width; x += m_pRactSize)
		{
			m_pRenderTarget->DrawLine(
				D2D1::Point2F(static_cast<FLOAT>(x), 0.0f),
				D2D1::Point2F(static_cast<FLOAT>(x), rtSize.height),
				m_pBlackBrush,
				0.5f
			);
		}

		for (int y = 0; y < height; y += m_pRactSize)
		{
			m_pRenderTarget->DrawLine(
				D2D1::Point2F(0.0f, static_cast<FLOAT>(y)),
				D2D1::Point2F(rtSize.width, static_cast<FLOAT>(y)),
				m_pBlackBrush,
				0.5f
			);
		}

		hr = m_pRenderTarget->EndDraw();
	}

	if (hr == D2DERR_RECREATE_TARGET)
	{
		hr = S_OK;
		DiscardDeviceResources();
	}

	return hr;
}

void LifeGameWindow::RunMessageLoop()
{
	MSG msg = {0};

	bool new_message;

	while ((!m_pRenderStopped && ((new_message = PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) || true))) || (new_message = GetMessage(&msg, NULL, 0, 0)))
	{
		if (new_message) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);

			if (msg.message == WM_QUIT)
				break;
		}

		if (!m_pRenderStopped)
		{
			m_pGameEngine->RecomputeState();
			OnRender();
		}
	}
}

void LifeGameWindow::DiscardDeviceResources()
{
	SafeRelease(&m_pBlackBrush);
	SafeRelease(&m_pRenderTarget);
	SafeRelease(&m_pGreenBrush);
	SafeRelease(&m_pWhiteBrush);
}

void LifeGameWindow::OnResize(UINT width, UINT height)
{
	if (m_pRenderTarget)
	{
		// Note: This method can fail, but it's okay to ignore the
		// error here, because the error will be returned again
		// the next time EndDraw is called.
		m_pRenderTarget->Resize(D2D1::SizeU(width, height));
	}
}

