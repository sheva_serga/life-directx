#include "basewindow.h"
#include "game_ingine.h"
#include <d2d1.h>
#include <thread>
#include <mutex>

#pragma once
class LifeGameWindow : public BaseWindow<LifeGameWindow> {

private:
	bool m_pRenderStopped;
	UINT m_pRactSize;

	ID2D1Factory* m_pDirect2dFactory;
	ID2D1HwndRenderTarget* m_pRenderTarget;
	ID2D1SolidColorBrush* m_pBlackBrush;
	ID2D1SolidColorBrush* m_pGreenBrush;
	ID2D1SolidColorBrush* m_pWhiteBrush;
	LifeGameEngine* m_pGameEngine;

public:
	LifeGameWindow(LifeGameEngine* m_pGameEngine, UINT ractsize);

	~LifeGameWindow();

	BOOL Initialize(
		PCWSTR lpWindowName,
		DWORD dwStyle,
		int nWidth,
		int nHeight,
		DWORD dwExStyle = 0,
		int x = 0,
		int y = 0,
		HWND hWndParent = 0,
		HMENU hMenu = 0
	);


	PCWSTR ClassName() const;
	LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);

	HRESULT CreateDeviceIndependentResources();
	HRESULT CreateDeviceResources();
	HRESULT OnRender();

	void RunMessageLoop();
	void DiscardDeviceResources();
	void OnResize(UINT width, UINT height);
};

