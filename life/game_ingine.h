#include <windows.h>

#pragma once
class LifeGameEngine
{
private: 
	bool** prev_state; 
	bool** current_state;

public:
	UINT turn, x_size, y_size;


	LifeGameEngine(UINT x, UINT y);
	~LifeGameEngine();

	void RecomputeState();
	void Clear();
	void SetLifeToArea(UINT x, UINT y);

	bool** GetCurrentState();
};

