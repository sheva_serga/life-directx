#include "pch.h"
#include "game_ingine.h"

#include <windows.h>
#include <ppl.h>

using namespace concurrency;

inline int GetVirtualPointCordinate(int p_local, int p_max) 
{
	return p_local < 0
		? p_max + (p_local + 1)
		: p_local > p_max
			? p_max - (p_local - 1)
			: p_local;
}

LifeGameEngine::LifeGameEngine(UINT x, UINT y) : x_size(x), y_size(y), turn(0)
{
	prev_state = new bool*[y];
	
	for (size_t i = 0; i < y; i++)
		prev_state[i] = (bool*)calloc(x, sizeof(bool));

	current_state = new bool*[y];

	//initialize state by some defauld values
	for (size_t i = 0; i < y; i++)
		current_state[i] = (bool*)calloc(x, sizeof(bool));
}

LifeGameEngine::~LifeGameEngine()
{
	for (size_t i = 0; i < y_size; i++) 
	{
		delete[] prev_state[i];
		delete[] current_state[i];
	}

	delete[] prev_state;
	delete[] current_state;
}

void LifeGameEngine::RecomputeState()
{
	int x_max = x_size - 1;
	int y_max = y_size - 1;

	concurrency::parallel_for(0, (int)y_size, [&](int y) {
		for (int x = 0; x < x_size; x++)
		{
			UINT lifeNear = 0;

			for (int local_y = y - 1; local_y < y + 2; local_y++)
			{
				for (int local_x = x - 1; local_x < x + 2; local_x++)
				{
					if (local_y == y && local_x == x) continue;

					int y_virt_value, x_virt_value;

					y_virt_value = GetVirtualPointCordinate(local_y, y_max);
					x_virt_value = GetVirtualPointCordinate(local_x, x_max);

					lifeNear += current_state[y_virt_value][x_virt_value] ? 1 : 0;
				}
			}

			prev_state[y][x] = (current_state[y][x] && (lifeNear == 2 || lifeNear == 3))
				|| (!current_state[y][x] && lifeNear == 3);
		}
	});

	// swap 2 states
	bool** temp = current_state;
	current_state = prev_state;
	prev_state = temp;

	turn += 1;
}


void LifeGameEngine::Clear() 
{
	concurrency::parallel_for(0, (int)y_size, [&](int y) {
		for(int x = 0; x < x_size; x++)
			current_state[y][x] = false;
	});
}


void LifeGameEngine::SetLifeToArea(UINT x, UINT y)
{
	current_state[y][x] = true;
}

bool ** LifeGameEngine::GetCurrentState()
{
	return current_state;
}
