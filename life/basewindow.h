#include <windows.h>
#include <windowsx.h>

#pragma once
template <class DERIVED_TYPE>
class BaseWindow
{
public:
	static LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		DERIVED_TYPE *pThis = NULL;

		if (uMsg == WM_NCCREATE)
		{
			CREATESTRUCT* pCreate = (CREATESTRUCT*)lParam;
			pThis = (DERIVED_TYPE*)pCreate->lpCreateParams;
			SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)pThis);

			pThis->m_hwnd = hwnd;
		}
		else
		{
			pThis = (DERIVED_TYPE*)GetWindowLongPtr(hwnd, GWLP_USERDATA);
		}

		return pThis 
			? pThis->HandleMessage(uMsg, wParam, lParam) 
			: DefWindowProc(hwnd, uMsg, wParam, lParam);
	}

	BaseWindow() : m_hwnd(NULL) { }

	virtual BOOL Initialize(
		PCWSTR lpWindowName,
		DWORD dwStyle,
		int nWidth,
		int nHeight,
		DWORD dwExStyle = 0,
		int x = 0,
		int y = 0,
		HWND hWndParent = 0,
		HMENU hMenu = 0
	) {
		WNDCLASSEX wc = { 0 };

		ZeroMemory(&wc, sizeof(WNDCLASS));

		wc.lpfnWndProc = DERIVED_TYPE::WindowProc;
		wc.hInstance = GetModuleHandle(NULL);
		wc.lpszClassName = ClassName();
		wc.cbSize = sizeof(WNDCLASSEX);
		wc.style = CS_HREDRAW | CS_VREDRAW;
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = (HBRUSH)COLOR_WINDOW;

		RegisterClassEx(&wc);

		RECT wr = { 0, 0, nWidth, nHeight };    // set the size, but not the position
		AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, FALSE);

		m_hwnd = CreateWindowEx(
			dwExStyle, 
			ClassName(), 
			lpWindowName, 
			dwStyle, 
			x, 
			y,
			wr.right - wr.left,    // width of the window
			wr.bottom - wr.top,    // height of the window
			hWndParent, 
			hMenu, 
			GetModuleHandle(NULL), 
			this
		);

		return (m_hwnd ? TRUE : FALSE);
	}

	HWND Window() const { return m_hwnd; }

protected:

	virtual PCWSTR  ClassName() const = 0;
	virtual LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam) = 0;

	HWND m_hwnd;
};

